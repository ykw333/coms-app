package com.wcr;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * @author 최의신 (choies@kr.ibm.com)
 *
 */
public class MainActivity extends AppCompatActivity {
    public class WebInterface {
        /**
         * 카메라 실행
         */
        @JavascriptInterface
        public void captureImage() {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra("android.intent.extra.quickCapture",true);
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Toast.makeText(MainActivity.this, "이미지 처리 오류! 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                finish();
                e.printStackTrace();
            }
            if (photoFile != null) {
                photoUri = FileProvider.getUriForFile(MainActivity.this,
                        "hana.provider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intent, PICK_FROM_CAMERA);
            }
        }

        /**
         * 앨범에서 선택
         */
        @JavascriptInterface
        public void loadImage() {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
            startActivityForResult(intent, PICK_FROM_ALBUM);
        }
    }

    /**
     * OCR 타스크
     */
    private class OcrTask extends AsyncTask<Uri, Void, Void> {
        ProgressDialog asyncDialog = new ProgressDialog( MainActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            asyncDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            asyncDialog.setMessage("OCR 처리중.."); // show dialog
            asyncDialog.show();
        }

        @Override
        protected Void doInBackground(Uri... image) {
            /*
              OCR 요청
             */
            try {
                // OCR 처리 이미지로 변환
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), image[0]);
                Bitmap rsImg = resizeBitmapImage(bitmap, 800);
                Bitmap bwImg = convertToBlackWhite(rsImg);
                saveBitmaptoJpeg(bwImg, image[0]);

                String imgFilename = (new File(image[0].toString())).getName();
                Log.d(TAG, "@.@ START OCR : " + imgFilename);

                byte [] imageBytes = toBytes(image[0]);
                RequestBody requestFile = RequestBody.create(MediaType.parse(getContentResolver().getType(image[0])), imageBytes);
                MultipartBody.Part body = MultipartBody.Part.createFormData("image", imgFilename, requestFile);

                OcrInterface ocrInterface = RetrofitApiClient.getClient(OCR_SERVICE_URL).create(OcrInterface.class);

                Call<ResultVO> resultCall = ocrInterface.uploadImage(body);

                final String ocrTxt = (new Gson()).toJson(resultCall.execute().body());

                runOnUiThread(() -> {
                    myBrowser.loadUrl("javascript:setOcrResult('" + ocrTxt + "')");
                });
            } catch (Exception e) {
                Log.e(TAG, "error: " + e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            asyncDialog.dismiss();
            super.onPostExecute(result);
        }
    }

    private static final String OCR_SERVICE_URL = "http://169.56.80.74:8888/";

    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_ALBUM = 2;
    private static final int CROP_FROM_CAMERA = 3;

    private final String TAG = getClass().getSimpleName();

    private WebView myBrowser;
    private Uri photoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myBrowser = (WebView)findViewById(R.id.mybrowser);
        myBrowser.getSettings().setDefaultTextEncodingName("UTF-8");

        // 6.0 마쉬멜로우 이상일 경우에는 권한 체크 후 권한 요청
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "@.@ 권한 설정 완료");
            } else {
                Log.d(TAG, "@.@ 권한 설정 요청");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        /**
         * HTML 페이지 인터페이스
         */
        final WebInterface myJavaScriptInterface = new WebInterface();
        myBrowser.addJavascriptInterface(myJavaScriptInterface, "AndroidFunction");

        myBrowser.getSettings().setJavaScriptEnabled(true);
        myBrowser.loadUrl(OCR_SERVICE_URL + "ocr.html");
    }

    // 권한 요청
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "@.@ onRequestPermissionsResult");
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "@.@ Permission: " + permissions[0] + "was " + grantResults[0]);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ( requestCode == PICK_FROM_ALBUM ) {
            if (resultCode != RESULT_OK) {
                Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (data == null) {
                return;
            }
            photoUri = data.getData();
            cropImage();
        }
        else if ( requestCode == PICK_FROM_CAMERA ) {
            if (resultCode != RESULT_OK) {
                Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
                return;
            }

            cropImage();
        }
        else if ( requestCode == CROP_FROM_CAMERA ) {
            revokeUriPermission(photoUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            try {
                OcrTask task = new OcrTask();
                task.execute(photoUri);
            }catch(Exception e) {
                e.printStackTrace();
                myBrowser.loadUrl("javascript:setOcrResult('이미지가 없습니다.')");
            }
        }
    }

    /**
     *
     * @param source
     * @param maxResolution
     * @return
     */
    public Bitmap resizeBitmapImage(Bitmap source, int maxResolution)
    {
        int width = source.getWidth();
        int height = source.getHeight();
        int newWidth = width;
        int newHeight = height;
        float rate = 0.0f;

        if(width > height)
        {
            if(maxResolution < width)
            {
                rate = maxResolution / (float) width;
                newHeight = (int) (height * rate);
                newWidth = maxResolution;
            }
        }
        else
        {
            if(maxResolution < height)
            {
                rate = maxResolution / (float) height;
                newWidth = (int) (width * rate);
                newHeight = maxResolution;
            }
        }

        return Bitmap.createScaledBitmap(source, newWidth, newHeight, true);
    }

    /**
     *
     * @param src
     * @return
     */
    private Bitmap convertToBlackWhite(Bitmap src)
    {
        Bitmap dest = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());

        for(int x = 0; x < src.getWidth(); x++){
            for(int y = 0; y < src.getHeight(); y++){
                int pixelColor = src.getPixel(x, y);
                int pixelAlpha = Color.alpha(pixelColor);
                int pixelRed = Color.red(pixelColor);
                int pixelGreen = Color.green(pixelColor);
                int pixelBlue = Color.blue(pixelColor);

                int pixelBW = (pixelRed + pixelGreen + pixelBlue)/3;
                int newPixel = Color.argb(pixelAlpha, pixelBW, pixelBW, pixelBW);

                dest.setPixel(x, y, newPixel);
            }
        }

        return dest;
    }

    /**
     *
     */
    public void cropImage()
    {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(photoUri, "image/*");
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        grantUriPermission(list.get(0).activityInfo.packageName, photoUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(this, "취소 되었습니다.", Toast.LENGTH_SHORT).show();
            return;
        } else {
            Toast.makeText(this, "용량이 큰 사진의 경우 시간이 오래 걸릴 수 있습니다.", Toast.LENGTH_SHORT).show();
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 5);
            intent.putExtra("aspectY", 3);
            intent.putExtra("scale", true);

            intent.putExtra("return-data", false);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());

            Intent i = new Intent(intent);
            ResolveInfo res = list.get(0);
            grantUriPermission(res.activityInfo.packageName, photoUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            startActivityForResult(i, CROP_FROM_CAMERA);
        }
    }

    /**
     *
     * @param image
     * @return
     */
    private byte [] toBytes(Uri image)
    {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();

        try {
            byte [] buffer = new byte[1024];
            int n = 0;
            InputStream in = getContentResolver().openInputStream(image);

            do {
                n = in.read(buffer);
                if ( n > 0 )
                    bo.write(buffer, 0, n);
            }while(n > 0);
        }catch(Exception e) {
            e.printStackTrace();
        }

        return bo.toByteArray();
    }

    /**
     *
     * @param bitmap
     * @param imageUrl
     */
    public void saveBitmaptoJpeg(Bitmap bitmap, Uri imageUrl)
    {
        try {
            ContentResolver cr = getContentResolver();
            OutputStream out = cr.openOutputStream(imageUrl);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        }catch(FileNotFoundException exception){
            Log.e("FileNotFoundException", "@.@ " + exception.getMessage());
        }catch(IOException exception){
            Log.e("IOException", "@.@ " + exception.getMessage());
        }
    }

    /**
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        String imageFileName = "ocrtest_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStorageDirectory() + "/HanaOCR/");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        return image;
    }
}
