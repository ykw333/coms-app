package com.wcr;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 최의신 (choies@kr.ibm.com)
 *
 */
public class ResultVO {
	private String userName;
	private String phoneNumber;
	private String mobileNumber;
	private String email;
	private String cardImage;
	private String parsedText;
	private List<String> lineText;

	public String getCardImage() {
		return cardImage;
	}

	public void setCardImage(String cardImage) {
		this.cardImage = cardImage;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getParsedText() {
		return parsedText;
	}

	public void setParsedText(String parsedText) {
		this.parsedText = parsedText;
	}

	public List<String> getLineText() {
		return lineText;
	}

	public void setLineText(List<String> lineText) {
		this.lineText = lineText;
	}

	public ResultVO() {
		lineText = new ArrayList<String>();
		userName = "";
		phoneNumber = "";
		mobileNumber = "";
		email = "";
	}
}
