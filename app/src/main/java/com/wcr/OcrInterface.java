package com.wcr;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * @author 최의신 (choies@kr.ibm.com)
 *
 */
public interface OcrInterface {
    @Multipart
    @POST("ocr")
    Call<ResultVO> uploadImage(@Part MultipartBody.Part image);
}